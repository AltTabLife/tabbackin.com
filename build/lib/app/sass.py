import sass
import os

css_dirs = {
    './static/css',
    './static/sass'
}

try:
    for folder in css_dirs:
        os.makedirs(folder)
        print(f"Made folder: {folder}")
except OSError:
    continue



