from flask import Blueprint, render_template

bp = Blueprint('home_page', __name__)

@bp.route('/')
def index():
    return render_template('home_page/index.html')