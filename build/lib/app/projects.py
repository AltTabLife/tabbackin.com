from flask import Blueprint, render_template
import markdown, re

from .extensions import pages
bp = Blueprint('projects', __name__)


@bp.route('/projects/')
def projects():
    recent_articles = sorted(pages, reverse=True, key=lambda page: page.meta['date'])[:5]
    return render_template('projects/projects.html', recent_articles=recent_articles)

def create_nav_content(markdown_content):
    headers = []
    updated_lines = []  # Create a list to store the updated lines

    for line in markdown_content.splitlines():
        if line.startswith('<h3>'):
            header_text = re.sub(r'<.*?>', '', line).strip()
            header_id = re.sub(r'[^a-zA-Z0-9]+', '-', header_text).lower()
            headers.append({'text': header_text, 'id': header_id})
            # Replace the original line with the line containing the anchor link
            line = f'<h3 id="{header_id}">{header_text}</h3>'
        updated_lines.append(line)  # Add the updated line to the list

    # Join the updated lines back into the full HTML content
    html_content = '\n'.join(updated_lines)
    html_content = markdown.markdown(html_content)

    return html_content, headers

@bp.route('/projects/<path:path>.html')
def project_article(path):
    page = pages.get_or_404(path)
    
    #Create the Page Navigator content
    html_content, headers = create_nav_content(page.body)
    return render_template('projects/base_article.html', page=page, html_content=html_content, page_nav=headers)