const sideNav = document.getElementById('SideNav');
const toggleButton = document.getElementById('toggleButton');

let isNavOpen = false;

toggleButton.addEventListener('click', () => {
  if (isNavOpen) {
    sideNav.style.display = 'none';
    toggleButton.textContent = '>';
  } else {
    sideNav.style.display = 'block';
    toggleButton.textContent = '<';
  }
  isNavOpen = !isNavOpen;
});
