import os
from flask import Flask
from .extensions import pages
from sassutils.wsgi import SassMiddleware
from .config import SECRET_KEY

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY=SECRET_KEY,
        DATABASE=os.path.join(app.instance_path,'tabbackin.sqlite'),
        FLATPAGES_ROOT='pages',
        FLATPAGES_EXTENSION='.md'
    )
    app.wsgi_app = SassMiddleware(app.wsgi_app, {
        'app': ('static/sass', 'static/css', '/static/css')
    })
    
    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass



    pages.init_app(app)

    @app.route('/hello')
    def hello():
        return 'You found me!'
    
    from . import home_page
    app.register_blueprint(home_page.bp)

    from . import projects
    app.register_blueprint(projects.bp)

    return app