---
title: TabBackIn
date: 07-21-2023
date_updated: 07-21-2023
---

Welcome to the journey of creating TabBackIn (This very website) and learning through what is needed to make what you see today. Triumphs, mistakes, and how lessons are learned in the development.

<h3>Why</h3>
That is the simplest question to answer. Don't overthink it. I'm personally not a fan of social media's vapid time sink and needed a way to showcase my work. What better way to do it than a personal website?

<h3>Where to start</h3>
While it doesn't always show from the github, my website development in general has gone through probably 4 different phases that I can remember, moving from one to the next as inefficiencies were too cumbersome. Each phase I wouldn't call a failure, moreso a learning process that makes me appreciate just how far computers have come.

* Raw HTML/CSS in notepad++ (stopped due to sheer amount of coding for just a few pages)
* Modifying template sites (stopped due to CSS being waaayyy to complicated and hard to navigate from sheer number of classes, not to mention a LOT of modification just to make it look exactly how I wanted it to)
* Saying screw it and moving to SquareSpace (while an option for some, I hated the limited choices on how it could look, with no way to fit in custom live feeds, api creation, or anything that pops other than a "yep. it's here" style)
* Python-Flask (The current iteration, which combines my knowledge of python programmability, with Jinja that stops the heavy HTML/CSS gripes from before)

<h3>Lessons Learned</h3>
So what were the lessons learned with each phase?

1. RTFM  
Probably the biggest lesson out of all of this is to *read the fucking manual.*
GeeksforGeeks, Dev, StackOverflow, openai, and Youtube are all great sources to help learn something, but they should all be used as *secondary* resources. I've wasted countless hours watching youtube tutorials on flask organization, reading how to add flatpages from dev, and conversing with gpt3 on how to solve errors, all while palletprojects put out a comprehensive example project on how to make an application factory.
<br>

2. If you have no experience, you aren't original.  
It's a bit contradictory in the common thought process that I've seen in myself and my generation, that the thought of an "outside perspective" means that original ideas can be made and inherently valuable. The more experience I gain in a particular subject though, the more that logic proves wrong for two big reasons. The first being, without intricate knowledge on how something *can* work, you're most likely going to reinvent the wheel. The second, being a play on the first, is that there are so many minds out there, *someone* has thought of what you want to create, so there *most likely* is a tool that someone has made to utilize it.
<br>

3. Complicated ideas don't get things done.  
A bit of an analogy to the car world, why would you overengineer an LS4 swap to a Chevy cruze when the Impala exists? If you have a complicated master plan that's going to be the next big thing, take it 3 steps back and find the easier way to implement it. It might not be completely efficient, but the biggest advantage of working with computers in the modern age is that processing power is cheap. Who cares if you lose 1.5 seconds on something that isn't being ran more than a hundred times per day? Processing power at anything but the highest tiers is an easy pay to win.
<br>

4. All the Git  
A bit of a noob one this is, but one I had to learn the hard way. You can only remember so much on what you just modified before it takes starting completely over from the last save state to get it right, but what if you don't have a last save state? Good luck trying to get the errors to stop. Git all of your files/projects (nobody said they had to be uploaded). If you don't want to take the advice, try adjusting a complicated standalone parser into a class with relevant functions into a module while having import errors due to VSCode being buggy due to importing without a good working save state to go back to. Go ahead. I'll wait.